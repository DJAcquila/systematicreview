import json
import argparse

# Refer to: https://stackoverflow.com/a/5921708
def intersperse(lst, item):
    result = [item] * (len(lst) * 2 - 1)
    result[0::2] = lst
    return result

class FileStructure:
    def __init__(self, file) -> None:
        self.file = file
        self.description:str = self.get_description()
        self.keys:dict = self.get_keys()
        self.restrictions:dict = self.get_restrictions()
        self.database:dict = self.get_database()

    def get_database(self):
        try:
            return self.file["database"]
        except Exception as e:
            print("Please provide database for your search strings")
            raise e

    def get_description(self):
        try:
            return self.file["description"]
        except Exception:
            print("File without description... No problem")
            return ""

    def get_keys(self):
        try:
            return self.file["keys"]
        except Exception as e:
            print("Please provide keys for your search strings")
            raise e

    def get_restrictions(self):
        try:
            return self.file["restrictions"]
        except Exception as e:
            print("Please provide restrictions for your search strings")
            raise e

    def filter_by_keys(self, selected:list):
        if selected == []:
            return

        self.keys = {key: value for key, value in self.keys.items() if key in selected}

    def filter_by_restrictions(self, selected:list):
        if selected == []:
            self.restrictions = {}
            return

        self.restrictions = {key: value for key, value in self.restrictions.items() if key in selected}


class FileHandler:
    def __init__(self, filename:str, keys:list, restrictions:list, database:str) -> None:
        self.filename:str = filename
        self.file = None
        self.selected_keys:list = keys
        self.selected_restrictions:list = restrictions
        self.database_item:str = database

        self.json_file:FileStructure = None

        self.search_string:str = ""
        
    def run(self)->str:
        self.parse_json_file()
        print("Starting new search string creator for '{}'".format(self.json_file.description))
        if self.database_item:
            print("\nImporting database search string '{}'".format(self.database_item))
            self.selected_keys = self.json_file.database[self.database_item]["keys"]
            self.selected_restrictions = self.json_file.database[self.database_item]["restrictions"]

        print("\nApplying filters...")
        self.apply_filters()

        print("\nGenerating Search String...")
        generated_string = self.generate_search_string()
        print("\t-> Search string: {}".format(generated_string))
    
    @staticmethod
    def apply_or_statements(list_of_keys:list)->str:  
        list_of_keys[0] = "(" + list_of_keys[0]
        list_of_keys[-1] = list_of_keys[-1] + ")" 
        return " OR ".join(list_of_keys)

    @staticmethod
    def apply_and_statements(list_of_keys:list)->str:        
        return " AND ".join(list_of_keys)

    def generate_search_string(self):
        elements = [FileHandler.apply_or_statements(self.json_file.keys[k]) for k in self.selected_keys]
        
        return FileHandler.apply_and_statements(elements)


    def apply_filters(self):
        print("Filtering keys...")
        self.json_file.filter_by_keys(self.selected_keys)
        print("\t-> New keys:\n\t\t {}".format(self.json_file.keys))

        print("Filtering restrictions...")
        self.json_file.filter_by_restrictions(self.selected_restrictions)
        print("\t-> New restrictions:\n\t\t {}".format(self.json_file.restrictions))
    
    def parse_json_file(self):
        if not self.is_json_file():
            print("Bad structured file")
            return 

        file = self.open_file()
        self.json_file = FileStructure(json.loads(file.read()))
    
    def open_file(self):
        try:
            return open(self.filename)
        except OSError as e: 
            print("Error opening the file")
            raise e

    def is_json_file(self):
        splitted_file = None
        try:
            splitted_file = self.filename.split('.')
        except Exception as e:
            print("Error parsing file")
            raise e
        else:
            if len(splitted_file) != 2 or splitted_file[-1] != 'json':
                return False
            return True



if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--filename", required=True, help="Json File Name")
    ap.add_argument("-d", "--database", help="Use database. Must provide the identifier in database")
    ap.add_argument("-k", "--keys", nargs="+", default=[])
    ap.add_argument("-r", "--restrictions", nargs="+", default=[])

    args = vars(ap.parse_args())

    handler = FileHandler(args['filename'], args['keys'], args['restrictions'], args['database'])
    handler.run()