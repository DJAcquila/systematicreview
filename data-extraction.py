import argparse
from os import stat
import pandas as pd
from pandas.core.frame import DataFrame

class DataTypes:
    ARTICLES = "articles"
    DATA_EXTRACTION = "data_extraction"


class ArticlesHandler:
    def __init__(self):
        pass

class DataExportHandler:
    def __init__(self) -> None:
        self.articles = None
        self.data_extraction = None
        

    def add_file(self, file_structure:dict):
        if DataTypes.ARTICLES in file_structure :
            self.articles = file_structure[DataTypes.ARTICLES]
            # print(self.articles.keys())
        elif DataTypes.DATA_EXTRACTION in file_structure:
            self.data_extraction = file_structure[DataTypes.DATA_EXTRACTION]
        else:
            print("DataType not supported")

    def get_accepted_articles(self):
        return self.articles[self.articles['status'] == "Accepted"]

    def get_rejected_articles(self):
        return self.articles[self.articles['status'] == "Rejected"]

    def get_duplicated_articles(self):
        return self.articles[self.articles['status'] == "Duplicated"]
    
    @staticmethod
    def get_title(df):
        return df['title']

    def generate_article_reports(self):
        def journal_per_status(self):
            grouped_df = self.articles.groupby('journal')['status'].value_counts().unstack().fillna(0)
            grouped_df.to_csv("./results/journal_per_status.csv")
            return grouped_df

        def publisher_per_status(self):
            grouped_df = self.articles.groupby('publisher')['status'].value_counts().unstack().fillna(0)
            grouped_df.to_csv("./results/publisher_per_status.csv")
            return grouped_df

        def year_per_status(self):
            grouped_df = self.articles.groupby('year')['status'].value_counts().unstack().fillna(0)
            grouped_df.to_csv("./results/year_per_status.csv")
            return grouped_df


        def articles_per_status(self):
            grouped_df = self.articles.groupby('status')['title'].count()
            grouped_df.to_csv("./results/articles_per_status.csv")
            return grouped_df

        def articles_per_year(self):
            grouped_df = self.articles.groupby('year')['title'].count()
            grouped_df.to_csv("./results/articles_per_year.csv")
            return grouped_df

        def accepted_articles(self):
            grouped_df = self.get_accepted_articles()[['title', 'author', 'journal', 'year']]
            grouped_df.to_csv("./results/accepted_articles.csv")
            return grouped_df

        def accepted_articles_per_year(self):
            accepted:DataFrame = self.get_accepted_articles()
            grouped_df = accepted.groupby('year')['title'].count()
            grouped_df.to_csv("./results/accepted_articles_per_year.csv")
            return grouped_df

        def accepted_articles_per_journal(self):
            accepted:DataFrame = self.get_accepted_articles()
            grouped_df = accepted.groupby('journal')['title'].count().fillna(0)
            grouped_df.to_csv("./results/accepted_articles_per_journal.csv")
            return grouped_df

        def accepted_articles_per_source(self):
            accepted:DataFrame = self.get_accepted_articles()
            grouped_df = accepted.groupby('source')['title'].count().fillna(0)
            grouped_df.to_csv("./results/accepted_articles_per_source.csv")
            return grouped_df


        def rejected_articles(self):
            grouped_df = self.get_rejected_articles()[['title', 'author', 'journal', 'year']]
            grouped_df.to_csv("./results/rejected_articles.csv")
            return grouped_df

        def rejected_articles_per_year(self):
            rejected:DataFrame = self.get_rejected_articles()
            grouped_df = rejected.groupby('year')['title'].count()
            grouped_df.to_csv("./results/rejected_articles_per_year.csv")
            return grouped_df

        def rejected_articles_per_journal(self):
            rejected:DataFrame = self.get_rejected_articles()
            grouped_df = rejected.groupby('journal')['title'].count().fillna(0)
            grouped_df.to_csv("./results/rejected_articles_per_journal.csv")
            return grouped_df


        def duplicated_articles(self):
            grouped_df = self.get_duplicated_articles()[['title', 'author', 'journal', 'year']]
            grouped_df.to_csv("./results/duplicated_articles.csv")
            return grouped_df

        def duplicated_articles_per_year(self):
            duplicated:DataFrame = self.get_duplicated_articles()
            grouped_df = duplicated.groupby('year')['title'].count()
            grouped_df.to_csv("./results/duplicated_articles_per_year.csv")
            return grouped_df

        def duplicated_articles_per_journal(self):
            duplicated:DataFrame = self.get_duplicated_articles()
            grouped_df = duplicated.groupby('journal')['title'].count().fillna(0)
            grouped_df.to_csv("./results/duplicated_articles_per_journal.csv")
            return grouped_df


        articles_per_year(self)
        articles_per_status(self)
        year_per_status(self)
        publisher_per_status(self)
        journal_per_status(self)

        # Accepted
        accepted_articles(self)
        accepted_articles_per_year(self)
        accepted_articles_per_journal(self)
        accepted_articles_per_source(self)

        # Rejected
        rejected_articles(self)
        rejected_articles_per_year(self)
        rejected_articles_per_journal(self)

        # Duplicated
        duplicated_articles(self)
        duplicated_articles_per_year(self)
        duplicated_articles_per_journal(self)

class FileHandler:
    def __init__(self, articles_file:str, data_extraction_file:str):
        self.articles_file:str = articles_file
        self.data_extraction_file:str = data_extraction_file
        self.data_handler = DataExportHandler()
        self.prepare()

    def prepare(self):
        self.data_handler.add_file(self._parse_csv_file(self.articles_file, DataTypes.ARTICLES))
        self.data_handler.add_file(self._parse_csv_file(self.data_extraction_file, DataTypes.DATA_EXTRACTION))

    def generate_report(self):
        self.data_handler.generate_article_reports()
    
    @staticmethod
    def _parse_csv_file(filename:str, type:str):
        try:
            return {type: pd.read_csv(filename, sep=";")}
        except Exception as e:
            raise e
    
    @staticmethod
    def _open_file(filename):
        try:
            return open(filename)
        except OSError as e: 
            print("Error opening the file")
            raise e

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-fa", "--articles", default=["./results/parsifal/articles.csv"], help="CSV articles file")
    ap.add_argument("-fd", "--data_extraction", default=["./results/parsifal/data_extraction.csv"], help="CSV data extraction file")
    args = vars(ap.parse_args())

    fh = FileHandler(args['articles'][0], args['data_extraction'][0])
    fh.generate_report()