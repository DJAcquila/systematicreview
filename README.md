# SystematicReview

### Create new search string based on keys

```
python3 search-string-generator.py -f search-string-db.json -k SE1 SE2 SE3 SE4
```

### Generate BASE pre-defined search string
```
python3 search-string-generator.py -f search-string-db.json -d BASE

```

### Generate IEEE pre-defined search string
```
python3 search-string-generator.py -f search-string-db.json -d IEEE

```